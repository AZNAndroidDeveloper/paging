package uz.azn.paging.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import uz.azn.paging.model.PhotosItem
import uz.azn.paging.databinding.ImageRowItemBinding as ImageBinding

class ImageAdapter : PagedListAdapter<PhotosItem, ImageAdapter.ViewHolder>(MyDiffUtil()) {

    class MyDiffUtil : DiffUtil.ItemCallback<PhotosItem>() {
        override fun areItemsTheSame(oldItem: PhotosItem, newItem: PhotosItem): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: PhotosItem, newItem: PhotosItem): Boolean {
            return oldItem == newItem
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ImageBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(getItem(position)!!)
    }

    inner class ViewHolder(private val binding: ImageBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind(photo:PhotosItem){
            Picasso.get().load(photo.src.medium).into(binding.imageView)
        }

    }

}