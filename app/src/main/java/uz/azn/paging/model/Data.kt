package uz.azn.paging.model

import com.google.gson.annotations.SerializedName

data class Data(

	@field:SerializedName("next_page")
	val nextPage: String,

	@field:SerializedName("per_page")
	val perPage: Int,

	@field:SerializedName("page")
	val page: Int,

	@field:SerializedName("photos")
	val photos: List<PhotosItem>,

	@field:SerializedName("total_results")
	val totalResults: Int
)