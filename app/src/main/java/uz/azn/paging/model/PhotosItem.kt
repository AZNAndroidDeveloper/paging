package uz.azn.paging.model

import com.google.gson.annotations.SerializedName

data class PhotosItem(

	@field:SerializedName("src")
	val src: Src,

	@field:SerializedName("width")
	val width: Int,

	@field:SerializedName("avg_color")
	val avgColor: String,

	@field:SerializedName("photographer")
	val photographer: String,

	@field:SerializedName("photographer_url")
	val photographerUrl: String,

	@field:SerializedName("id")
	val id: Int,

	@field:SerializedName("url")
	val url: String,

	@field:SerializedName("photographer_id")
	val photographerId: Int,

	@field:SerializedName("liked")
	val liked: Boolean,

	@field:SerializedName("height")
	val height: Int
)