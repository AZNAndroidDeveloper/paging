package uz.azn.paging

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import uz.azn.paging.adapter.ImageAdapter
import uz.azn.paging.databinding.FragmentIntroBinding
import uz.azn.paging.viewmodel.ImageViewModels

class IntroFragment : Fragment(R.layout.fragment_intro) {
    private lateinit var  binding: FragmentIntroBinding
    private lateinit var imageViewModels: ImageViewModels
    private lateinit var imageAdapter:ImageAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding =  FragmentIntroBinding.bind(view)
        imageAdapter = ImageAdapter()
        imageViewModels = ViewModelProviders.of(requireActivity())[ImageViewModels::class.java]
        binding.etSearch.addTextChangedListener {
            imageViewModels.getSearchImages(it.toString()).observe(requireActivity(), Observer {
                //Diff util classnikini
                imageAdapter.submitList(it)
            })
        }
        binding.recyclerView.layoutManager = GridLayoutManager(requireContext(),2)
        binding.recyclerView.adapter = imageAdapter

    }
}