package uz.azn.paging.retrofit

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query
import uz.azn.paging.model.Data

interface ApiService {
    @GET("search")
    fun getImages(
        @Header("Authorization") auth: String = RetrofitClient.API_KEY,
        @Query("query") query: String = "People",
        @Query("page") page: Int
    ): Call<Data>
}