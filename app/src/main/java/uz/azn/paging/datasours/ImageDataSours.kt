package uz.azn.paging.datasours

import androidx.paging.PageKeyedDataSource
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import uz.azn.paging.model.Data
import uz.azn.paging.model.PhotosItem
import uz.azn.paging.retrofit.ApiService
import uz.azn.paging.retrofit.RetrofitClient

class ImageDataSours(val search:String) : PageKeyedDataSource<Int, PhotosItem>() {
    val apiService = RetrofitClient.getRetrofit().create(ApiService::class.java)
    val START_PAGE = 1
    val PAGE_SIZE = 50
    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, PhotosItem>
    ) {
        apiService.getImages(page = START_PAGE,query = search).enqueue(object : Callback<Data> {
            override fun onFailure(call: Call<Data>, t: Throwable) {
            }

            override fun onResponse(call: Call<Data>, response: Response<Data>) {
                if (response.isSuccessful) {
                    callback.onResult(response.body()!!.photos, null, START_PAGE + 1)
                }
            }

        })


    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, PhotosItem>) {
        apiService.getImages(page = params.key,query = search ).enqueue(object : Callback<Data> {
            override fun onFailure(call: Call<Data>, t: Throwable) {
            }

            override fun onResponse(call: Call<Data>, response: Response<Data>) {
                if (response.isSuccessful) {
                    val nextPageKey: Int? = if (params.key == PAGE_SIZE) {
                        params.key + 1

                    } else {
                        null
                    }
                    callback.onResult(response.body()!!.photos, nextPageKey)
                }
            }

        })
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, PhotosItem>) {

    }
}