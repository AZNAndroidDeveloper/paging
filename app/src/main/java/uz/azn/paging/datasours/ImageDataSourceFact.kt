package uz.azn.paging.datasours

import androidx.paging.DataSource
import uz.azn.paging.model.PhotosItem


class ImageDataSourceFact(val search:String) : DataSource.Factory<Int, PhotosItem>() {
    private lateinit var imageDataSource: ImageDataSours
    override fun create(): DataSource<Int, PhotosItem> {
        imageDataSource = ImageDataSours(search)
        return imageDataSource
    }

}