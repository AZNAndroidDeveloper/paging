package uz.azn.paging.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import uz.azn.paging.datasours.ImageDataSourceFact
import uz.azn.paging.model.PhotosItem
import java.util.concurrent.Executor
import java.util.concurrent.Executors

class ImageViewModels : ViewModel() {
//    var imagePagedList: LiveData<PagedList<PhotosItem>>? = null
//    var executor: Executor? = null

    init {
//        val imageDataSourceFact = ImageDataSourceFact()
//        executor = Executors.newSingleThreadExecutor()
//        // pageni nastroykasini qilish
//        val pagedListConfig = PagedList.Config.Builder()
//            .setEnablePlaceholders(true)
//            .setInitialLoadSizeHint(10)
//            .setPageSize(50)
//            .build()
//
//        imagePagedList = LivePagedListBuilder<Int, PhotosItem>(imageDataSourceFact, pagedListConfig)
//            .setFetchExecutor(executor)
//            .build()
    }
    fun getSearchImages(query:String):LiveData<PagedList<PhotosItem>>{
        val imageDataSourceFact = ImageDataSourceFact(query)
        val executor: Executor?
        executor = Executors.newSingleThreadExecutor()
        // pageni nastroykasini qilish
        val pagedListConfig = PagedList.Config.Builder()
            .setEnablePlaceholders(true)
            .setInitialLoadSizeHint(10)
            .setPageSize(50)
            .build()

      return   LivePagedListBuilder<Int, PhotosItem>(imageDataSourceFact, pagedListConfig)
            .setFetchExecutor(executor)
            .build()
    }
}